# Records the voice of a user

# https://python-sounddevice.readthedocs.io/en/latest/
# https://python-sounddevice.readthedocs.io/en/0.4.3/installation.html
# conda install -c conda-forge python-sounddevice
# or
# python3 -m pip install sounddevice

# https://nitratine.net/blog/post/how-to-detect-key-presses-in-python/
# pip install pynput

import datetime  # to set filename using current date and time
import sounddevice as sd  # to use microphone to launch recording
import soundfile as sf  # to read .wav files
from scipy.io.wavfile import write  # to write wav file on disk
from pynput import keyboard  # to use keyboard listener
from src.app.display import display, display_attr, get_message

# import winsound                     # to play beep sound before recording voice
import os

"""Global variables"""

"""
frequency = 1200  # Set beep sound Frequency To 1200 Hertz
frequency2 = 400  # Set beep sound Frequency To 500 Hertz
duration = 1000  # Set beep sound Duration To 1000 ms
duration2 = 200  # Set beep sound Duration To 1000 ms
"""

output_dir = "src/data/wav_files/"  # wav file saving directory
fs = 48000  # Sample rate
seconds = 3  # Duration of recording

nb_recording_new_user = 5
nb_max_attempts = 3

files = []  # list of filenames during recordings of registering a new person
stop = False

files_auth = []  # list of filenames during recordings of authentification
stop_auth = False

file_test_identification = None # filename during recording of authentification test in main admin
stop_test_identification = False

beep_dir = "src/data/interactions/beep-yes.wav"

""" functions """


def record():
    """Function to launch the recording process """
    # winsound.Beep(frequency, duration)
    path = beep_dir
    recording, fs = sf.read(path)
    #sd.play(recording, fs)
    #sd.wait()

    myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
    display("record", "recording_in_progress")
    sd.wait()  # Wait until recording is finished
    # winsound.Beep(frequency2, duration2)
    display("record", "recording_complete")

    # creating filename string using current date and time
    filename = "record_"
    filename += (
        str(datetime.datetime.now())
        .replace(" ", "_")
        .replace(".", "-")
        .replace(":", "-")
    )
    filename += ".wav"

    # writing the wav file on disk
    output = output_dir + filename
    write(output, fs, myrecording)  # Save as WAV file

    return filename


def record_user(lastname, firstname):
    """function called to record the voice of a new user several times"""
    global files
    global stop
    stop = False

    files = []
    print(get_message("record", "recording_guide_part_1") + firstname + " " + lastname +
          get_message("record", "recording_guide_part_2"))
    for i in range(nb_recording_new_user):
        display_attr(
            "record",
            "recording_instruction",
            str(i + 1) + "/" + str(nb_recording_new_user),
        )
        with keyboard.Listener(on_press=on_press_admin) as listener:
            listener.join()
        if stop:
            remove_files(files)
            return False, []
    return True, files


def remove_files(files):
    for f in files:
        path = output_dir + f
        os.remove(path)


def on_press_admin(key):
    if key == keyboard.Key.esc:
        global stop
        stop = True
        return False  # stop listener
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys

    if k in ["enter"]:  # keys of interest
        file = record()
        global files
        files.append(file)
        return False  # stop listener; remove this if want more keys


def play_wav(filename):
    """function to play a .wav file located in the output_dir folder"""
    print(get_message("record", "playing_file") + filename)
    try:
        path = output_dir + filename
        recording, fs = sf.read(path)
        sd.play(recording, fs)
        sd.wait()
        return True
    except ValueError:
        print(ValueError)
        return False


"""

def on_press(key):
    if key == keyboard.Key.esc:
        global stop
        stop = True
        return False  # stop listener
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys
        
    if k in ['space', 'r', 'enter']:  # keys of interest
        file = record()
        global files 
        files.append(file)
        return False # stop listener; remove this if want more keys

def record_and_play():     
    output_dir = "./records/"
    fs = 48000   # Sample rate
    seconds = 3  # Duration of recording
    myrecording = sd.rec(int(seconds * fs), samplerate=fs, channels=2)
    print("Enregistrement en cours")
    sd.wait()  # Wait until recording is finished
    print("Enregistrement terminé")
    
    output = output_dir  
    output += 'record_'
    output += str(datetime.datetime.now()).replace(" ", "_").replace(".", "-").replace(":","-")
    output += ".wav"
    write(output, fs, myrecording)  # Save as WAV file 
    
    print("Lecture")
    sd.play(myrecording, fs)
    sd.wait()
"""


def record_auth(nb_attempt):
    """function called to record the voice of a person during access"""
    global stop_auth
    global files_auth
    stop_auth = False
    if nb_attempt == 1:
        files_auth = []

    print(
        get_message("record", "authentification_instruction")
        + str(nb_attempt)
        + "/"
        + str(nb_max_attempts)
        + ")"
    )
    with keyboard.Listener(on_press=on_press_auth) as listener:
        listener.join()
    if stop_auth:
        return False, files_auth
    return True, files_auth


def on_press_auth(key):
    if key == keyboard.Key.esc:
        global stop_auth
        stop_auth = True
        return False  # stop listener
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys

    if k in ["enter"]:  # keys of interest
        file = record()
        global files_auth
        files_auth.append(file)
        return False  # stop listener; remove this if want more keys


def record_test_identification():
    """function called to record the voice of a person during identification test in main admin"""
    global stop_test_identification
    global file_test_identification
    stop_test_identification = False

    print(get_message("record", "test_authentification_instruction"))
    with keyboard.Listener(on_press=on_press_test_iden) as listener:
        listener.join()
    if stop_test_identification:
        return False, file_test_identification
    return True, file_test_identification


def on_press_test_iden(key):
    if key == keyboard.Key.esc:
        global stop_test_identification
        stop_test_identification = True
        return False  # stop listener
    try:
        k = key.char  # single-char keys
    except:
        k = key.name  # other keys

    if k in ["enter"]:  # keys of interest
        global file_test_identification
        file_test_identification = record()
        return False  # stop listener


"""
listener = keyboard.Listener(on_press=on_press)
listener.start()  # start to listen on a separate thread
listener.join()  # remove if main thread is polling self.keys
"""
# record_user()
