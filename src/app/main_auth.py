# import winsound        # to play beep sound during authentification
import sounddevice as sd  # to use microphone to launch recording
import soundfile as sf  # to read .wav files
from datetime import datetime
from sys import exit

from src.app.display import display, get_message
from src.app.record.record import record_auth, remove_files
from src.app.data_management.annuaire import (
    update_last_access,
    search_user_by_id
)
from src.app.data_management.blacklist import (
    save_black_listed_person,
    add_date_attempt_to_black_listed_person
)
from src.app.sound_processing.sound_extraction import extractVector
from src.app.sound_processing.identification import compare_to_directory, compare_to_blacklist

""" global variables """

nb_max_attempts = 3
frequency_success = 900
frequency_failure = 200
duration_success = 1000
duration_failure = 1000
fs = 48000  # Sample rate
beep_yes = "src/data/interactions/beep-yes.wav"
beep_no = "src/data/interactions/beep-no.wav"

""" functions """


def main():
    """main function to start the authentification program"""
    attempt = 1
    display("menu_auth", "welcome")
    vectors = []
    files = []
    dates = []
    while attempt <= nb_max_attempts:
        ok, files = record_auth(attempt)
        if ok == False:  # 'esc' was pressed
            print("")
            display("menu_auth", "exit")

            # add the wav files to a new blacklist entry if there are any
            #if files == []:
            #    exit()

            #save_black_listed_person(vectors, files, dates)
            
            exit()
        else:
            dates.append(datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
            # extract caract
            path = "src/data/wav_files/" + files[len(files) - 1]
            vector = extractVector(path)
            vectors.append(vector.tolist())
            # compare with blacklist
            res, _id_ = compare_to_blacklist(vector)
            if res == True:
                print("")
                display("menu_auth", "you_are_blacklisted")

                recording, fs = sf.read(beep_no)
                sd.play(recording, fs)
                sd.wait()
                # winsound.Beep(frequency_failure, duration_failure)

                # add the wav files to the blacklist entry
                add_date_attempt_to_black_listed_person(vectors, files, dates, _id_)
                return

            # compare with directory
            res, _id_ = compare_to_directory(vector)
            if res == True:
                user = search_user_by_id(_id_)
                if user["authorized"]:
                    print("")
                    display("menu_auth", "authentification_success")
                    print(get_message("menu_auth", "display_name") + " " + user["firstname"] + " " + user["lastname"] + "\n" ) 
                    recording, fs = sf.read(beep_yes)
                    sd.play(recording, fs)
                    sd.wait()
                    # winsound.Beep(frequency_success, duration_success)

                    # remove all the wav files
                    remove_files(files)
                    # update last_access_date to today for this id
                    update_last_access(_id_)
                    return
                else:
                    print("")
                    display("menu_auth", "authentification_success_but_denied")

                    recording, fs = sf.read(beep_no)
                    sd.play(recording, fs)
                    sd.wait()
                    # winsound.Beep(frequency_failure, duration_failure)

                    # remove all the wav files
                    remove_files(files)
                    return
            else:
                attempt += 1
                if attempt > nb_max_attempts:
                    print("")
                    display("menu_auth", "authentification_failed_max_attempt")
                    # add the wav files to a new blacklist entry
                    save_black_listed_person(vectors, files, dates)

                else:
                    print("")
                    display("menu_auth", "authentification_failed_try_again")

                recording, fs = sf.read(beep_no)
                sd.play(recording, fs)
                sd.wait()
                # winsound.Beep(frequency_failure, duration_failure)
    return


############## INTEGRATION TESTING FUNCTIONS #############


# sound processing > sound extraction
"""
def extractVector(files):
    print("extracting vectors")
    return []
"""

# sound processing > identification

"""
def compare_to_blacklist(vector):

    # true if found and return the ID, else false, None
    print("comparaison avec blacklist")
    global criteria
    best_res = 999
    accept = False
    _id_ = -1
    users = search_all_black_listed_persons()
    for user in users:
        print("\ncomparaison à l'id " + str(user["id"]))
        for v in user["vocal_print"]:
            cost = compare_vectors(vector, v)
            print("cout : " + str(cost))
            if cost < criteria:
                accept = True
                if cost < best_res:
                    best_res = cost
                    _id_ = user["id"]
    return accept, _id_
"""

"""
def compare_to_directory(vector):
    # true if found and return the ID, else false, None
    print("comparaison avec annuaire")
    global criteria
    best_res = 999
    accept = False
    _id_ = -1
    users = search_all_users()
    for user in users:
        print("\ncomparaison à l'id " + str(user["id"]))
        for v in user["vocal_print"]:
            cost = compare_vectors(vector, v)
            print("cout : " + str(cost))
            if cost < criteria:
                accept = True
                if cost < best_res:
                    best_res = cost
                    _id_ = user["id"]
    return accept, _id_

"""

# data management > annuaire
"""
def update_last_access(_id_):
    print("updating last accessed for id " + str(_id_) )
    return 
"""

# data management > blacklist
"""
def add_date_attempt_to_black_listed_person(vectors, files, dates, _id_):
    print("adding files to blacklist at id " + str(_id_) + " - " + str(files))
    return
"""
"""
def save_black_listed_person(vectors, files, dates):
    print("adding files to new blacklist entry " + str(files) )
    return
"""


"""execution """

while True:
    main()
