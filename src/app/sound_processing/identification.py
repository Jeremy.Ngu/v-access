"""File that holds every steps to identify a person from a wav file"""
import math as mth
import numpy as np
import librosa as lbrs
import matplotlib.pyplot as plt

import sklearn.metrics.pairwise as sk


# pylint: disable=E0401
from src.app.data_management.annuaire import search_all_users
from src.app.data_management.blacklist import search_all_black_listed_persons
from src.app.sound_processing.sound_extraction import extractVector

# Tries to match the comparison with best_of values
BEST_OF = 3
# Threshold which is recognized as similar vector
THRESHOLD_MEAN_COST = 52000
THRESHOLD_BEST_COST = 12
WEIGHT_BEST_COST = 1.0
WEIGHT_MEAN_COST = 1.0


def compare_vectors(v1, v2, debug=False, returnParam="both"):
    """Compare the similarity between 2 vectors. The lower is better
    Args:
        v1 ([[float]]): [matrix of mfccs]
        v2 ([[float]]): [matrix of mfccs]
        debug (bool, optional): [debug mode]. Defaults to False.
    Returns:
        float: similarity, lower is better
    """

    return compare_vectors_dtw(v1, v2, debug=debug, returnParam=returnParam)
    # return compare_vectors_cosine(v1, v2, debug=debug)


def compare_vectors_dtw(v1, v2, debug=False, returnParam="both"):
    """Compares the MFCCs of 2 vectors using the DTW method.
    Args:
        v1 ([[float]]): [matrix of mfccs of an audio]
        v2 ([[float]]): [matrix of mfccs of an audio]
    Returns:
        [float]: [distance between 2 vectors]
    """
    D, wp = lbrs.sequence.dtw(v1, v2, subseq=True)
    best_cost = D[wp[-1, 0], wp[-1, 1]]

    # Printing graph if debug is True
    if debug:
        print("Best cost : ")
        print(best_cost)

        print((np.array(D)).shape)

        fig, ax = plt.subplots(nrows=1, sharex=True)
        img = lbrs.display.specshow(D, x_axis="frames", y_axis="frames", ax=ax)
        ax.set(title="DTW cost", xlabel="Y sequence", ylabel="X sequence")
        ax.plot(wp[:, 1], wp[:, 0], label="Optimal path", color="y")
        ax.legend()
        fig.colorbar(img, ax=ax)
        plt.show()

    # Computing the accumulated cost
    total_cost = 0
    for i in range(len(wp[:, 1])):
        total_cost += mth.fabs(D[wp[i, 0], wp[i, 1]])
    mean_cost = total_cost / len(wp[:, 1])

    if debug:
        print("Shape : ", wp.shape)
        print("Mean cost : ", mean_cost)

    # Depending on what score we want to use, we can specify the return through returnParam
    if returnParam == "best":
        return best_cost
    if returnParam == "mean":
        return mean_cost
    if returnParam == "both":
        return (best_cost, mean_cost)
    return best_cost


def compare_vectors_cosine(v1, v2, debug=False):
    """Compare vectors using cosinus similarity
    Args:
        v1 ([[float]]): [matrix of mfcss]
        v2 ([[float]]): [matrix of mfcss]
        debug (bool, optional): [debug mode]. Defaults to False.
    Returns:
        float: similarity scorem lower is better.
    """

    if debug:
        print("Compare vectors using cosine similarity")
    v1_converted = convert_matrix_to_vector(v1)
    v2_converted = convert_matrix_to_vector(v2)
    return sk.distance.cosine(v1_converted, v2_converted)


def convert_matrix_to_vector(matrix):
    """Convert a matrix to a vector
    Args:
        matrix ([[float]]): [matrix of mfccs]
    Returns:
        [float]: [vector of mfcss]
    """
    vector = []
    for line in matrix:
        for value in line:
            vector.append(value)
    return vector


def compare_to_blacklist(vector, debug=False):
    """Compares the vector to the blacklist,
    returns if it belongs to the blacklist and the ID if so.
    Args:
        vector ([[float]]): [matrix of mfccs of an audio]
    Returns:
        [(boolean,int)]: [Belongs to the blacklist, ID]
    """

    # using both best cost and mean cost method
    bl = search_all_black_listed_persons()
    valid = []
    # For every person in the blacklist, we compare his vocal print to the current vector v1.
    for person in bl:
        scores = []
        for vb in person["vocal_print"]:
            if debug:
                print(np.array(vb).shape)
                print(np.array(vector).shape)

            best, mean = compare_vectors(vector, vb, returnParam="both")

            if debug:
                print("ID : " + str(person["id"]))
                print("Best cost : " + str(best))
                print("Mean cost : " + str(mean))

            # If best_score or mean score is acceptable, adds it to the list of scores.
            if cost_under_threshold(best, mean):
                scores.append(compute_score(best, mean))
        # If there's at least BEST_OF number of score acceptable, we consider that the person is recognized as blacklisted.
        if debug:
            print(scores)
        if len(scores) >= BEST_OF:
            valid.append((person["id"], sum(scores)))

    if len(valid) == 0:
        return False, -1

    sorted_valid_results = sorted(valid, key=lambda x: x[1])
    _id_, lowest_score = sorted_valid_results[0]
    if debug:
        print("lowest score : ", lowest_score)
    return True, _id_


# pylint: disable=undefined-loop-variable
def compare_to_directory(vector, debug=False):
    """Compares the vector to the DB, returns if it belongs to the DB and the ID if so.
    Args:
        vector ([[float]]): [matrix of mfccs of an audio]
    Returns:
        [(boolean,int]]: [Belongs to the annuaire, ID]
    """

    # using both best cost and mean cost method
    # Annuaire
    an = search_all_users()
    # List of valid scores
    valid = []
    # For every person in the annuaire, we compare his vocal print to the current vector v1.
    for person in an:
        scores = []
        # if debug: # checking all costs
        #    print("")
        #    print("ID : " + str(person["id"]))
        #    print("")

        for va in person["vocal_print"]:

            # if debug:    # checking vector array sizes
            # print(np.array(va).shape)
            # print(np.array(vector).shape)

            best, mean = compare_vectors(vector, va, returnParam="both")
            # if debug: # checking all costs
            #    print("Best cost : " + str(best))
            #    print("Mean cost : " + str(mean))
            #    print("")

            if cost_under_threshold(best, mean):
                if debug:  # checking costs under threshold
                    print("ID : " + str(person["id"]))
                    print("Best cost : " + str(best))
                    print("Mean cost : " + str(mean))
                    print("")

                scores.append(compute_score(best, mean))

        if len(scores) >= BEST_OF:
            valid.append((person["id"], sum(scores)))

    if debug:  # checking costs under threshold
        print("-------")
        print("")

    if len(valid) == 0:
        return False, -1

    sorted_valid_results = sorted(valid, key=lambda x: x[1])
    _id_, lowest_score = sorted_valid_results[0]
    if debug:
        print("lowest score : ", lowest_score)

    return True, _id_


def cost_under_threshold(best, mean):
    """returns true if either the best cost or the mean cost is valid"""
    if best < THRESHOLD_BEST_COST or mean < THRESHOLD_MEAN_COST:
        return True
    return False


def compute_score(best_cost, mean_cost):
    """returns a score using both the best cost
    and the mean_cost of dtw identification.

    The score computed will always be lower if both thresholds are met
    than if only one threshold is met.
    The weight of each identification method can be parametrized

    Args:
        best_cost(float): distance between 2 vectors using best cost criteria
        mean_cost(float): distance between 2 vectors using mean cost criteria
    Returns:
        (float): computed score
    """

    total_weight = WEIGHT_BEST_COST + WEIGHT_MEAN_COST

    score = (
        WEIGHT_BEST_COST * (best_cost / THRESHOLD_BEST_COST) * 100
        + WEIGHT_MEAN_COST * (mean_cost / THRESHOLD_MEAN_COST) * 100
    )

    # if both methods return a valid result
    if best_cost <= THRESHOLD_BEST_COST and mean_cost <= THRESHOLD_MEAN_COST:
        return score

    # if only one method returns a valid result
    return score + (100 * total_weight)


def compare_to_list(vector, listToCompare, debug=False):
    """Compares a vector to a list of vectors

    Args:
        vector ([[float]]): matrix of mfccs
        listToCompare ([[[float]]]): list of matrix of mfccs
        debug (bool, optional): mode debug to print. Defaults to False.
    """

    # Stands for the number of matching vectors in the current person
    cpt = 0
    # Stands for the index of vector to a person,
    # A person contains 5 vectors.
    cpt_person = 0
    for person in listToCompare:
        # If we compared 5 vectors, set it up to a new person to identify
        if cpt_person + 1 % 5 == 0:
            cpt_person = 0
            cpt = 0
        best, mean = compare_vectors(vector, person, debug=debug, returnParam="both")
        if debug:
            print("SCORE : ", best, mean)
        # If best cost or mean cost is correct, we increment the number of matching vector
        if best < THRESHOLD_BEST_COST or mean < THRESHOLD_MEAN_COST:
            cpt += 1
        cpt_person += 1

        # If we matched the correct amonth of vector to a person, return true
        if cpt >= BEST_OF:
            return True
    return False


"""

BELLOW THIS LINE IS DEVELOPMENT PHASE TESTING

"""


# pylint: disable=too-many-locals
# pylint: disable=too-many-branches
def test():
    """Run tests for the identification component
    Using the DTW method.
    """
    # pylint: disable=too-many-locals,too-many-branches
    print("Identification")

    # Extracting vectors from wavs
    vp1 = extractVector("src/data/wav_files/acces_paul_nguyen_1.wav")
    vp2 = extractVector("src/data/wav_files/acces_paul_nguyen_2.wav")
    vp3 = extractVector("src/data/wav_files/acces_paul_nguyen_3.wav")
    vp4 = extractVector("src/data/wav_files/acces_paul_nguyen_4.wav")
    vp5 = extractVector("src/data/wav_files/acces_paul_nguyen_5.wav")

    vt1 = extractVector("src/data/wav_files/acces_thibault_roche_1.wav")
    vt2 = extractVector("src/data/wav_files/acces_thibault_roche_2.wav")
    vt3 = extractVector("src/data/wav_files/acces_thibault_roche_3.wav")
    vt4 = extractVector("src/data/wav_files/acces_thibault_roche_4.wav")
    vt5 = extractVector("src/data/wav_files/acces_thibault_roche_5.wav")

    va1 = extractVector("src/data/wav_files/record_alexis1.wav")
    va2 = extractVector("src/data/wav_files/record_alexis2.wav")
    va3 = extractVector("src/data/wav_files/record_alexis3.wav")
    va4 = extractVector("src/data/wav_files/record_alexis4.wav")
    va5 = extractVector("src/data/wav_files/record_alexis5.wav")

    vc1 = extractVector("src/data/wav_files/record_corentin1.wav")
    vc2 = extractVector("src/data/wav_files/record_corentin2.wav")
    vc3 = extractVector("src/data/wav_files/record_corentin3.wav")
    vc4 = extractVector("src/data/wav_files/record_corentin4.wav")
    vc5 = extractVector("src/data/wav_files/record_corentin5.wav")

    vi1 = extractVector("src/data/wav_files/record_isabelle1.wav")
    vi2 = extractVector("src/data/wav_files/record_isabelle2.wav")
    vi3 = extractVector("src/data/wav_files/record_isabelle3.wav")
    vi4 = extractVector("src/data/wav_files/record_isabelle4.wav")
    vi5 = extractVector("src/data/wav_files/record_isabelle5.wav")

    # Creating lists from vectors
    lp = [vp1, vp2, vp3, vp4, vp5]
    lt = [vt1, vt2, vt3, vt4, vt5]
    la = [va1, va2, va3, va4, va5]
    lc = [vc1, vc2, vc3, vc4, vc5]
    li = [vi1, vi2, vi3, vi4, vi5]

    # Concatenation of lists of vectors
    list_vectors = lp + lt + li + la + lc

    # Comparing to list
    result = compare_to_list(lp[0], list_vectors, debug=True)
    print("\nResult : ", result)

    for index in range(len(list_vectors)):
        compare_to_list(list_vectors[index], list_vectors[index:], debug=True)


def test_compare_vectors_cosine():
    """Run tests for the component.
    This test is using the cosinus similarity vector.
    Running it shows that the cosinus similarity is not good enough to make a difference between 2 audios.
    """
    # v1 = extractVector(
    #     "src/data/wav_files/projet_capgemini_src_data_wav_files_record_2021-11-17_17-40-00-672568.wav"
    # )
    # v2 = extractVector(∏
    #     "src/data/wav_files/projet_capgemini_src_data_wav_files_record_2021-11-17_17-40-03-763210.wav"
    # )
    # v3 = extractVector(
    #     "src/data/wav_files/projet_capgemini_src_data_wav_files_record_2021-11-17_17-40-06-853930.wav"
    # )
    # v4 = extractVector(
    #     "src/data/wav_files/projet_capgemini_src_data_wav_files_record_2021-11-17_17-40-09-935786.wav"
    # )
    # v5 = extractVector(
    #     "src/data/wav_files/projet_capgemini_src_data_wav_files_record_2021-11-17_17-40-13-024148.wav"
    # )

    print("Identification")

    vp1 = extractVector("src/data/wav_files/acces_paul_nguyen_1.wav")
    vp2 = extractVector("src/data/wav_files/acces_paul_nguyen_2.wav")
    vp3 = extractVector("src/data/wav_files/acces_paul_nguyen_3.wav")
    vp4 = extractVector("src/data/wav_files/acces_paul_nguyen_4.wav")
    vp5 = extractVector("src/data/wav_files/acces_paul_nguyen_5.wav")

    vjp1 = extractVector("src/data/wav_files/acces_paul_nguyen_par_jeremy_1.wav")
    vjp2 = extractVector("src/data/wav_files/acces_paul_nguyen_par_jeremy_2.wav")

    vtp1 = extractVector("src/data/wav_files/acces_paul_nguyen_par_thibault_1.wav")
    vtp2 = extractVector("src/data/wav_files/acces_paul_nguyen_par_thibault_2.wav")
    vtp3 = extractVector("src/data/wav_files/acces_paul_nguyen_par_thibault_3.wav")
    vtp4 = extractVector("src/data/wav_files/acces_paul_nguyen_par_thibault_4.wav")
    vtp5 = extractVector("src/data/wav_files/acces_paul_nguyen_par_thibault_5.wav")

    # Concatenation of vectors in list for the voices of paul jeremy and thibault
    lp = [vp1, vp2, vp3, vp4, vp5]
    lj = [vjp1, vjp2]
    lt = [vtp1, vtp2, vtp3, vtp4, vtp5]

    # Trying to match paul voice on paul.
    minP = 50000
    i = 0
    for v in lp:
        for j in range(i + 1, len(lp)):
            if j != i:
                comp = compare_vectors_cosine(v, lp[j])
                if comp is not None:
                    if comp < minP:
                        minP = comp
        i = i + 1

    # Trying to match jeremy's voice on paul's
    minJ = 50000
    for vi in lj:
        for vj in lp:
            comp = compare_vectors_cosine(vi, vj)
            if comp is not None:
                if comp < minJ:
                    minJ = comp

    # Trying to match thibault's voice on jeremy's
    minT = 50000
    for vi in lt:
        for vj in lp:
            comp = compare_vectors_cosine(vi, vj, debug=False)
            if comp is not None:
                if comp < minT:
                    minT = comp

    # Trying to match thibault's voice on his own
    minTT = 50000
    example_t = lt.pop()
    for v in lt:
        comp = compare_vectors_cosine(v, example_t)
        if comp is not None:
            if comp < minTT:
                minTT = comp

    print("Min paul : " + str(minP))
    print("Min jeremy : " + str(minJ))
    print("Min thibault : " + str(minT))
    print("Min Thibault thibault : " + str(minTT))


def test3():
    """Run tests for the identification component
    Compare 2 audios to the database. Annuaire and Blacklist.
    """

    vtp1 = extractVector("src/data/wav_files/acces_paul_nguyen_par_thibault_1.wav")
    vp2 = extractVector("src/data/wav_files/acces_paul_nguyen_2.wav")
    print("Blacklist test : ", compare_to_blacklist(vtp1))
    print("Annuaire test : ", compare_to_directory(vp2))


# Decomment to run tests. Get at the root of projet_capgemini and run python -m src.app.sound_processing.identification
# Might have to change the audio files to run your tests.
# test()
# test_compare_vectors_cosine()
# test3()
