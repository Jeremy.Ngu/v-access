"""
    This component is used to perform sound extraction from a wav.
"""
# Packages used ----------------------------------------------------------------
import os
import numpy as np
import librosa
import librosa.display as ld
import librosa.feature as lf
import matplotlib
import matplotlib.pyplot as plt

# print("Librosa version : ", librosa.__version__)
# print("matplotlib version : ", matplotlib.__version__)
# print("numpy version : ", np.__version__)
sPathDir = os.path.dirname(__file__)


def extractVector(file, n_mfcc=13, hopLength=256, dctType=2, lifter=26, debug=False):
    """Returns a mfcc matrix corresponding to the data analysis of an audio.


    windowing = (seconds) * (sample rate) / (hop_length)

    Args:
        file (string): [path of the file]
        vectorLength (int, optional): [Number of lines of MFCC to produce
        (Length 256)]. Defaults to 13, meaning 13 * 256 mfccs.
        dctType (int, optional): [Discrete cosine transformation type]. Defaults to 2.
        lifter (int, optional): [apply liftering (cepstral filtering) to the MFCCs]. Defaults to 26.

    Returns:
        [[float]]: [matrix of mfccs of an audio x axis is time, y axis are mffcs]
    """
    aAudio, nSampleRate = librosa.load(file, mono=True)

    # Preemphasis seems to put accent on word pronounciation
    # aAudio = librosa.effects.preemphasis(aAudio, coef=0.97)

    if debug:
        print("Sample Rate : ", nSampleRate)
        _, ax = plt.subplots(nrows=6, sharex=True)
        ld.waveshow(aAudio, sr=nSampleRate, ax=ax[0])
        ax[0].set(title="Before norm")
        ax[0].label_outer()

    # Normalize
    aAudio = aAudio / np.max(np.abs(aAudio))
    if debug:
        ld.waveshow(aAudio, sr=nSampleRate, ax=ax[1])
        ax[1].set(title="After norm")
        ax[1].label_outer()

    # Splitting audio not needed for now, might be later.
    # listVectorsCoeff = []
    # aAudioList = librosa.effects.split(aAudio, top_db=30)

    # cpt = 3
    # for elem in aAudioList:
    #     if debug:
    #         ld.waveshow(aAudio[elem[0] : elem[1]], sr=nSampleRate, ax=ax[cpt])
    #         ax[cpt].set(title="audio number " + str(cpt - 2))
    #         ax[cpt].label_outer()

    #     aVectorsCoeff = lf.mfcc(
    #         aAudio[elem[0] : elem[1]],
    #         sr=nSampleRate,
    #         n_mfcc=5,
    #         dctType=dctType,
    #         lifter=lifter,
    #     )

    #     print(len(aVectorsCoeff))
    #     print(aVectorsCoeff)
    #     listVectorsCoeff.append(aVectorsCoeff)
    #     cpt += 1
    # return listVectorsCoeff

    # Not splitting audio
    aVectorsCoeff = lf.mfcc(
        aAudio,
        sr=nSampleRate,
        n_mfcc=n_mfcc,
        hop_length=hopLength,
        dct_type=dctType,
        lifter=lifter,
    )
    if debug:
        plt.show()
        librosa.display.specshow(aVectorsCoeff, sr=44000)
        plt.xlabel("Time")
        plt.ylabel("MFCC")
        plt.colorbar()
        plt.show()
    return aVectorsCoeff


def get_number_mfcc(vector):
    """Return the number of mfcc in a matrix of mfccs

    Args:
        vector ([[floats]]): [mfcc matrix of an audio]

    Returns:
        [int]: [length]
    """
    length = 0
    for i in vector:
        length += len(i)
    return length


def test():
    """Perform tests for the sound_extraction component."""

    # Extract vectors from an audio, might have to change the path
    extractVector("src/data/wav_files/acces_paul_nguyen_1.wav", debug=True)


# test()
