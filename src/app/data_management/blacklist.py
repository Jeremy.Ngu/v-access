import json
from datetime import datetime

FILE_DATABASE = "src/data/voice/blacklist.json"


def update_database_location(new_file_database):
    """ (String) -> (boolean)
    Returns: True if new_file_database is a valid path and is updated
    """
    global FILE_DATABASE
    try:
        old_copy = FILE_DATABASE
        FILE_DATABASE = new_file_database
        file = open(FILE_DATABASE, "r")
        file.close()
        return True
    except IOError as e:
        print(e)
        FILE_DATABASE = old_copy
        return False
    return False


def add_date_attempt_to_black_listed_person(sound_vector, wav_files, dates, id):
    """Add date attemp to access of black listed person
    Args:
        sound_vector ([[float]]): [matrix of mfccs of an audio]
        wav_files ([[string]]): [black listed person's recording files]
        dates ([[string]]): [dates of wav files]
        id ([int]): [black listed person's id]
    Returns:
        [boolean]: [true]
    """
    try:
        file = open(FILE_DATABASE, "r")
        json_object = json.load(file)
        file.close()
        for item in json_object:
            if item["id"] == id:
                item["datetime_attempt"].extend(dates)
                item["vocal_print"].extend(sound_vector)
                item["wav_files"].extend(wav_files)
                break
        file = open(FILE_DATABASE, "w")
        json.dump(json_object, file, sort_keys=True, indent=2)
        file.close()
    except IOError as e:
        print(e)
        return False
    return True


def save_black_listed_person(sound_vector, wav_files, dates):
    """Add new black listed person 
    Args:
        sound_vector ([[float]]): [matrix of mfccs of an audio]
        wav_files ([[string]]): [black listed person's recording files]
        dates ([[string]]): [dates of wav files]
    Returns:
        [boolean]: [true]
    """
    try:
        file = open(FILE_DATABASE, "r")
        json_object = json.load(file)
        file.close()
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        if len(json_object) == 0:
            id = 0
        else:
            id = json_object[len(json_object) - 1]["id"] + 1
        black_listed_person = {
            "id": id,
            "datetime_creation": dt_string,
            "datetime_attempt": dates,
            "vocal_print": sound_vector,
            "wav_files": wav_files,
        }
        json_object.append(black_listed_person)
        file = open(FILE_DATABASE, "w")
        json.dump(json_object, file, sort_keys=True, indent=2)
        file.close()
    except IOError as e:
        print(e)
        return False
    return True


def delete_black_listed_person(id):
    """Delete black listed person by id
    Args:
        id ([int]): [black listed person's id]
    Returns:
        [boolean]: [true]
    """
    try:
        file = open(FILE_DATABASE, "r")
        json_object = json.load(file)
        file.close()
        for i in range(len(json_object)):
            if json_object[i]["id"] == id:
                json_object.pop(i)
                break
        file = open(FILE_DATABASE, "w")
        json.dump(json_object, file, sort_keys=True, indent=2)
        file.close()
    except IOError as e:
        print(e)
        return False
    return True


def search_black_listed_person_by_id(id):
    """Search black listed person by id
    Args:
        id ([int]): [black listed person's id]
    Returns:
        [dic]: [user if exist else false]
    """
    file = open(FILE_DATABASE, "r")
    json_object = json.load(file)
    file.close()
    for item in json_object:
        if item["id"] == id:
            return item
    return False


def search_all_black_listed_persons():
    """Search all black listed persons in blacklist data base
    Returns:
        [dic]: [list of black listed persons]
    """
    file = open(FILE_DATABASE, "r")
    json_object = json.load(file)
    file.close()
    return json_object


# print(save_black_listed_person(["vocal"],["wav_files"]))
# print(delete_black_listed_person(1))
# print(add_date_attempt_to_black_listed_person(0))
# print(search_black_listed_person_by_id(0))
# print(search_all_black_listed_persons())
