import json
from datetime import datetime

FILE_DATABASE = "src/data/voice/annuaire.json"


def update_database_location(new_file_database):
    """ (String) -> (boolean)
    Returns: True if new_file_database is a valid path and is updated
    """
    global FILE_DATABASE
    try:
        old_copy = FILE_DATABASE
        FILE_DATABASE = new_file_database
        file = open(FILE_DATABASE, "r")
        file.close()
        return True
    except IOError:
        FILE_DATABASE = old_copy
        return False
    return False


def is_user_exist(id):
    """Check if user exist in annuaire
    Args:
        id ([int]): [user id]
    Returns:
        [boolean]: [true if user exist else false]
    """
    file = open(FILE_DATABASE, "r")
    json_object = json.load(file)
    file.close()
    for item in json_object:
        if item["id"] == id:
            return True
    return False


def change_permissions(id, permission):
    """ Change the permissions of a user
    Args:
        id ([int]): [user id]
        permission ([boolean]): [true to give permission, false to deny permission]
    Returns:
        [boolean]: [true]
    """
    try:
        file = open(FILE_DATABASE, "r")
        json_object = json.load(file)
        file.close()
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        for item in json_object:
            if item["id"] == id:
                item["authorized"] = permission
                item["datetime_last_update"] = dt_string
                break
        file = open(FILE_DATABASE, "w")
        json.dump(json_object, file, sort_keys=True, indent=2)
        file.close()
    except IOError:
        return False
    return True


def save_new_user(first_name, last_name, company_id, files, vectors):
    """add new user .
    Args:
        first_name ([string]): user's first name
        last_name ([string]): user's last name
        company_id ([string]): user's company id
        files ([[string]]) : [recording's files]
        vectors([[float]]) : [matrix of mfccs of an audio]
    Returns:
        [boolean]: [True]
    """
    try:
        file = open(FILE_DATABASE, "r")
        json_object = json.load(file)
        file.close()
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        if len(json_object) == 0:
            id = 0
        else:
            id = json_object[len(json_object) - 1]["id"] + 1
        user = {
            "id": id,
            "firstname": first_name,
            "lastname": last_name,
            "company_id": company_id,
            "datetime_creation": dt_string,
            "datetime_last_update": dt_string,
            "datetime_last_access": "-",
            "vocal_print": vectors,
            "wav_files": files,
            "authorized": True,
        }
        json_object.append(user)
        file = open(FILE_DATABASE, "w")
        json.dump(json_object, file, sort_keys=True, indent=2)
        file.close()
    except IOError as e:
        print(e)
        return False
    return True


def update_user_sound_recording(id, files, vectors):
    """Update user's sound recording.
    Args:
        id ([int]): [user id]
        files ([[string]]) : [recording's files]
        vectors([[float]]) : [matrix of mfccs of an audio]
    Returns:
        [boolean]: [True]
    """
    try:
        file = open(FILE_DATABASE, "r")
        json_object = json.load(file)
        file.close()
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        for item in json_object:
            if item["id"] == id:
                item["vocal_print"] = vectors
                item["wav_files"] = files
                item["datetime_last_update"] = dt_string
                break
        file = open(FILE_DATABASE, "w")
        json.dump(json_object, file, sort_keys=True, indent=2)
        file.close()
    except IOError as e:
        print(e)
        return False
    return True


def update_last_access(id):
    """Update user's last access.
    Args:
        id ([int]): [user id]
    Returns:
        [boolean]: [True]
    """
    try:
        file = open(FILE_DATABASE, "r")
        json_object = json.load(file)
        file.close()
        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        for item in json_object:
            if item["id"] == id:
                item["datetime_last_access"] = dt_string
                break
        file = open(FILE_DATABASE, "w")
        json.dump(json_object, file, sort_keys=True, indent=2)
        file.close()
    except IOError as e:
        print(e)
        return False
    return True


def search_user_by_id(id):
    """Search a user by.
    Args:
        id ([int]): [user id]
    Returns:
        [dic]: [user]
    """
    file = open(FILE_DATABASE, "r")
    json_object = json.load(file)
    file.close()
    for item in json_object:
        if item["id"] == id:
            return item
    return False


def search_all_users():
    """Search all users.
    Returns:
        [dic]: [all users]
    """
    file = open(FILE_DATABASE, "r")
    json_object = json.load(file)
    file.close()
    return json_object


def search_users_by_keywords(str):
    """Search all users who have first name or last name or company id contains 
    the string str such as all elements are case-insensitive.
    Args:
        str ([string]): [searched string]
    Returns:
        [dic]: [list of users]
    """
    result = []
    file = open(FILE_DATABASE, "r")
    json_object = json.load(file)
    file.close()
    str = str.lower()
    for item in json_object:
        if (
            str in item["company_id"].lower()
            or str in item["firstname"].lower()
            or str in item["lastname"].lower()
        ):
            result.append(item)
    return result


# print(is_user_exist(5))
# print(change_permissions(0, False))
# print(save_new_user("K", "NH", "C02", ["file_name.wav"],["sound_record"]))
# print(update_user_sound_recording(0, ["files"],["sound_record_updating"]))
# print(search_user_by_id(1))
# print(search_all_users())
# print(search_users_by_keywords("NHA"))
