# Display messages in terminal

# Note: in function json.load, encoding="utf-8" is incompatible with (python > 3.9)

import json

"""Global variables"""

TRANSLATION_PATH = "src/data/lang/translation.json"
LANGUAGE = "fr"

""" functions """


def display(category, message):
    """display the corresponding message in the selected LANGUAGE"""
    file = open(TRANSLATION_PATH, "rb")
    # messages = json.load(file, encoding="utf-8")
    messages = json.load(file)
    file.close()
    print(str(messages[category][message][LANGUAGE]))


def display_attr(category, message, attribute):
    """display the corresponding message in the selected LANGUAGE along with the atrribute parameter"""
    file = open(TRANSLATION_PATH, "rb")
    # messages = json.load(file, encoding="utf-8")
    messages = json.load(file)
    file.close()
    print(str(messages[category][message][LANGUAGE]) + ": " + str(attribute))


def get_message(category, message):
    """return the corresponding message in the selected LANGUAGE"""
    file = open(TRANSLATION_PATH, "rb")
    # messages = json.load(file, encoding="utf-8")
    messages = json.load(file)
    file.close()
    return messages[category][message][LANGUAGE]


def change_language(choice):
    """change the global variable LANGUAGE,
    if choice is not recognized default to french"""
    global LANGUAGE
    if choice == "fr":
        LANGUAGE = choice
    elif choice == "en":
        LANGUAGE = choice
    else:
        LANGUAGE = "fr"


def display_user(item):

    _id_ = item["id"]
    lastname = item["lastname"]
    firstname = item["firstname"]
    registration_id = item["company_id"]
    authorization = item["authorized"]
    creation_date = item["datetime_creation"]
    last_modif_date = item["datetime_last_update"]
    last_access_date = item["datetime_last_access"]
    wav_files = item["wav_files"]
    columns_width = [5, 40]

    print(get_message("user", "id") + " : " + str(_id_))
    print(get_message("user", "firstname") + " : " + str(firstname))
    print(get_message("user", "lastname") + " : " + str(lastname))
    print(get_message("user", "registration_id") + " : " + str(registration_id))

    if authorization == True:
        print(
            get_message("user", "authorization")
            + " : "
            + get_message("user", "authorization_yes")
        )
    else:
        print(
            get_message("user", "authorization")
            + " : "
            + get_message("user", "authorization_no")
        )
    print(get_message("user", "creation_date") + " : " + str(creation_date))
    print(get_message("user", "last_modified") + " : " + str(last_modif_date))
    print(get_message("user", "last_access") + " : " + str(last_access_date))
    print(get_message("user", "wav_files"))
    for i in range(len(wav_files)):
        print(("{:" + str(columns_width[0]) + "}").format(str(i + 1)), end=" : ")
        print(("{:" + str(columns_width[1]) + "}").format(wav_files[i]))

    print("")
    return


def display_blacklisted_person(_id_, list_access, list_audio_files):
    """display informations related to blacklisted person
    listing all recordings from most recent to oldest
    """
    print(get_message("blacklist", "id") + " : " + str(_id_))
    print(str(len(list_access)) + get_message("blacklist", "recording_found"))
    columns_width = [5, 25, 40]
    for i in reversed(range(len(list_access))):
        print(
            ("{:" + str(columns_width[0]) + "}").format(str((len(list_access) - i))),
            end=" : ",
        )
        print(("{:" + str(columns_width[1]) + "}").format(list_access[i]), end=" | ")
        print(("{:" + str(columns_width[2]) + "}").format(list_audio_files[i]))

    print("")
    return


def list_users(users):
    columns_width = [5, 22, 22, 9, 18, 21, 21]
    # columns names
    print(
        ("{:" + str(columns_width[0]) + "}").format(get_message("user", "id")),
        end=" | ",
    )
    print(
        ("{:" + str(columns_width[1]) + "}").format(get_message("user", "lastname")),
        end=" | ",
    )
    print(
        ("{:" + str(columns_width[2]) + "}").format(get_message("user", "firstname")),
        end=" | ",
    )
    print(
        ("{:" + str(columns_width[3]) + "}").format(get_message("user", "reg_number")),
        end=" | ",
    )
    print(
        ("{:" + str(columns_width[4]) + "}").format(get_message("user", "autho")),
        end=" | ",
    )
    print(
        ("{:" + str(columns_width[5]) + "}").format(get_message("user", "last_modif")),
        end=" | ",
    )
    print(
        ("{:" + str(columns_width[6]) + "}").format(
            get_message("user", "last_acc") + "\n"
        )
    )

    for elem in users:
        # extract fields from the elem to print

        # trim the name and first name to fit in the list
        _id_ = str(elem["id"])
        lastname = elem["lastname"][: columns_width[1]]
        firstname = elem["firstname"][: columns_width[2]]
        if elem["authorized"] == True:
            authorized = get_message("user", "authorization_yes")
        else:
            authorized = get_message("user", "authorization_no")
        company_id = elem["company_id"]


        print(("{:" + str(columns_width[0]) + "}").format(_id_), end=" | ")
        print(("{:" + str(columns_width[1]) + "}").format(lastname), end=" | ")
        print(("{:" + str(columns_width[2]) + "}").format(firstname), end=" | ")
        print(("{:" + str(columns_width[3]) + "}").format(company_id), end=" | ")
        print(("{:" + str(columns_width[4]) + "}").format(authorized), end=" | ")
        print(
            ("{:" + str(columns_width[5]) + "}").format(elem["datetime_last_update"]),
            end=" | ",
        )
        print(("{:" + str(columns_width[6]) + "}").format(elem["datetime_last_access"]))

    print("")
    return True


def list_blacklist(people):

    columns_width = [5, 30, 18]
    # columns names
    print(
        ("{:" + str(columns_width[0]) + "}").format(get_message("blacklist", "id")),
        end=" | ",
    )
    print(
        ("{:" + str(columns_width[1]) + "}").format(
            get_message("blacklist", "last_access_attempt")
        ),
        end=" | ",
    )
    print(
        ("{:" + str(columns_width[2]) + "}").format(
            get_message("blacklist", "nb_attempts") + "\n"
        )
    )

    """
    class blacklisted_person:
        def __init__(self, _id , list_access=[], list_audio_files=[], vocal_characteristics=[]):
            self._id, self.list_access, self.list_audio_files, self.vocal_characteristics = _id, list_access, list_audio_files, vocal_characteristics

    d1 = datetime.datetime(2020, 12, 10, 23 , 48, 59, 213548)
    d2 = datetime.datetime(2020, 9, 20, 21 , 48, 59, 848)
    d3 = datetime.datetime(2021, 5, 1, 20 , 18, 0, 48)
    d4 = datetime.datetime(2021, 12, 31, 13 , 48, 59, 213548)

    b = blacklisted_person(46, [str(d1), str(d1)], ["a1","a2"], [])
    c = blacklisted_person(13, [str(d1), str(d2), str(d2), str(d3)], ["a1","a2"], [])
    d = blacklisted_person(26, [str(d1), str(d3), str(d4)], ["a1","a2"], [])
    e = blacklisted_person(47, [str(d1), str(d4), str(d2), str(d2), str(d2), str(d2)], ["a1","a2"], [])
    list_p = [b,c,d,e]

    """
    for elem in people:
        # some action on the blacklist
        # extract fields from the elem to print
        nb_elem = len(elem["datetime_attempt"])
        print(("{:" + str(columns_width[0]) + "}").format(str(elem["id"])), end=" | ")
        print(
            ("{:" + str(columns_width[1]) + "}").format(
                str(elem["datetime_attempt"][nb_elem - 1])
            ),
            end=" | ",
        )
        print(("{:" + str(columns_width[2]) + "}").format(str(nb_elem)))

    print("")
