import datetime
import sys
import os
import shutil  # for copy files
import easygui  # for file browser, pip install easygui
from src.app.display import (
    display,
    display_attr,
    change_language,
    get_message,
    display_user,
    display_blacklisted_person,
    list_users,
    list_blacklist,
)
from src.app.record.record import (
    record_user,
    play_wav,
    remove_files,
    record_test_identification,
)
from src.app.data_management.annuaire import (
    save_new_user,
    change_permissions,
    update_user_sound_recording,
    search_user_by_id,
    search_all_users,
    search_users_by_keywords,
)
from src.app.data_management.blacklist import (
    search_black_listed_person_by_id,
    search_all_black_listed_persons,
    delete_black_listed_person,
)
from src.app.sound_processing.sound_extraction import extractVector
from src.app.sound_processing.identification import compare_to_directory, compare_to_blacklist



""" functions """


def main():
    """main function to start the admin program"""
    display("menu_admin", "welcome")
    choice = main_menu()

    if choice == "1":
        record_new_user()
        main()

    if choice == "2":
        menu_directory()

    if choice == "3":
        menu_blacklist()

    if choice == "4":
        test_identification()
        main()

    if choice == "5":
        select_language()
        main()

    if choice == "q":
        sys.exit()


def main_menu():
    """display the main menu options and returns the user's choice"""
    display("menu_admin", "menu")
    choice = input()
    valid_choices = ["1", "2", "3", "4", "5", "q"]

    if choice in valid_choices:
        return choice

    else:
        display("error", "input_error")
        return main_menu()


####################### ADD NEW USER ###################


def record_inputs():
    """records the lastname, firstname and registration number of the new user"""
    display("menu_admin", "input_lastname")
    lastname = input()
    display("menu_admin", "input_firstname")
    firstname = input()
    display("menu_admin", "input_registration_id")
    registration_id = input()
    if registration_id == "":
        registration_id = "VISITOR"
    
    return lastname, firstname, registration_id


def confirm_inputs(lastname, firstname, registration_id):
    """function to confirm that the inputs are without error before recording user's voice"""
    display("menu_admin", "confirm_inputs")
    display_attr("menu_admin", "confirm_lastname", lastname)
    display_attr("menu_admin", "confirm_firstname", firstname)
    display_attr("menu_admin", "confirm_registration_id", registration_id)
    display("menu_admin", "confirm_inputs_2")
    
    while True:
        choice = input()
        yes = get_message("menu_admin", "press_yes")
        no = get_message("menu_admin", "press_no")
        valid_choices = [yes, no, "r"]
        if choice in valid_choices:
            if choice == yes:
                return False, lastname, firstname, registration_id
            elif choice == no:
                lastname, firstname, registration_id = record_inputs()
                return confirm_inputs(lastname, firstname, registration_id)
            elif choice == "r":
                return True, None, None, None

        else:
            display("error", "input_error")


def record_new_user():
    """function to record new user, record voice, and save user in directory"""
    # input user info
    lastname, firstname, registration_id = record_inputs()
    _return_, lastname, firstname, registration_id = confirm_inputs(
        lastname, firstname, registration_id
    )
    if _return_:
        display("menu_admin", "return_to_previous_menu")
        return

    choice = menu_record_method_display()
    if choice == "1":
        # record voice sounds
        ok, files = record_user(lastname, firstname)
        if ok == False:
            display("menu_admin", "return_to_previous_menu")
            return

    elif choice == "2":
        files = []
        files2 = easygui.fileopenbox(
            title="Importing sound files", msg="Select sound files", multiple=True
        )
        if files2 == None:
            display("menu_admin", "return_to_previous_menu")
            return

        i = 0
        for file in files2:
            filename = "record_"
            filename += (
                str(datetime.datetime.now())
                .replace(" ", "_")
                .replace(".", "-")
                .replace(":", "-")
            )
            filename += str(i)+".wav"
            i = i + 1
            path = os.getcwd() + "/src/data/wav_files/" + filename
            shutil.copy(file, path)
            files.append(filename)

    # function for extracting vocal caracteristics here
    display("menu_admin", "extracting")
    vectors = []
    for file in files:
        path = "src/data/wav_files/" + file
        vectors.append((extractVector(path)).tolist())
    display("menu_admin", "extracted")

    # functions to identify the vector in either the blacklist or the directory
    for vector in vectors:    
        found, _id = compare_to_blacklist(vector)
        if found:
            print("")
            print(get_message("error", "matched") + str(_id))
            display("error", "already_in_blacklist")
            remove_files(files)
            return
        
    for vector in vectors:
        found, _id = compare_to_directory(vector, debug=False)
        if found:
            print("")
            print(get_message("error", "matched") + str(_id))
            display("error", "already_in_directory")
            remove_files(files)
            return

    display("menu_admin", "saving")

    # function to save data in user directory here
    ok = save_new_user(firstname, lastname, registration_id, files, vectors)

    if ok == False:
        display("error", "error_during_save")
        remove_files(files)
        return

    display("menu_admin", "saved")

    if registration_id == "Visitor":
        registration_id = get_message("user", "visitor")
    print(
        get_message("menu_admin", "saved_user_1")
        + "("
        + firstname
        + " "
        + lastname
        + " - "
        + registration_id
        + ") "
        + get_message("menu_admin", "saved_user_2")
        + (datetime.datetime.now()).strftime("%d/%m/%Y %H:%M:%S")
    )
    print("")
    return


def menu_record_method_display():
    """ask the admin if the user's voice will be added using live recording or using sound files"""
    display("menu_admin", "record_option")
    choice = input()
    valid_choices = ["1", "2"]

    if choice in valid_choices:
        return choice

    else:
        display("error", "input_error")
        return menu_record_method_display()


####################### USER DIRECTORY ###################


def menu_directory_display():
    """display the user directory menu options and returns the user's choice"""
    display("menu_admin", "menu_directory")
    choice = input()
    valid_choices = ["1", "2", "3", "r"]

    if choice in valid_choices:
        return choice

    else:
        display("error", "input_error")
        return menu_directory_display()


def menu_directory():

    choice = menu_directory_display()
    if choice == "1":
        select_user_by_id()
        main()
    if choice == "2":
        search_user()
        menu_directory()
    if choice == "3":
        menu_sort_users(search_all_users())
        menu_directory()
    if choice == "r":
        main()


def menu_sort_users_display():
    """menu displayed when choosing to list users"""
    display("menu_admin", "sort_list_users")
    choice = input()
    valid_choices = ["1", "2", "3", "4", "5"]
    if choice in valid_choices:
        return choice

    else:
        display("error", "input_error")
        return menu_sort_users_display()
    return


def menu_sort_users(users):
    choice = menu_sort_users_display()

    # sort by ID
    if choice == "1":
        sorted_users = sorted(users, key=lambda x: x["id"])
        list_users(sorted_users)
        menu_directory()

    # sort by lastname
    if choice == "2":
        sorted_users = sorted(users, key=lambda x: x["lastname"])
        list_users(sorted_users)
        menu_directory()

    # sort by firstname
    if choice == "3":
        sorted_users = sorted(users, key=lambda x: x["firstname"])
        list_users(sorted_users)
        menu_directory()

    # sort by company ID
    if choice == "4":
        sorted_users = sorted(users, key=lambda x: x["company_id"])
        list_users(sorted_users)
        menu_directory()

    # sort by last access
    if choice == "5":

        def f(x):
            if x["datetime_last_access"] == "-":
                return datetime.datetime.min
            else:
                return datetime.datetime.strptime(
                    x["datetime_last_access"], "%d/%m/%Y %H:%M:%S"
                )

        sorted_users = sorted(users, key=lambda x: f(x), reverse=True)
        list_users(sorted_users)
        menu_directory()


def select_user_by_id():
    display("menu_admin", "enter_ID")
    _id_ = input()
    # isfound, lastname, firstname, registration_id, authorization, creation_date, date_last_modification, date_last_access = search_id_in_directory_bouchon(_id_)
    wav_files, lastname, firstname = display_user_by_id(_id_)
    menu_profile(_id_, wav_files, lastname, firstname)


def display_user_by_id(_id_):
    item = search_user_by_id(int(_id_))

    if item == False:
        print(
            get_message("error", "user_not_found_begin")
            + str(_id_)
            + get_message("error", "user_not_found_end")
        )
        return menu_directory()
    display_user(item)
    return item["wav_files"], item["lastname"], item["firstname"]


def menu_profile_display(wav_files):
    """display the user profile menu options and returns the user's choice"""
    display("menu_admin", "menu_profile")
    choice = input()
    yes = get_message("menu_admin", "press_yes")
    no = get_message("menu_admin", "press_no")
    valid_int = list(range(1, len(wav_files) + 1))
    valid_choices = [yes, no, "v", "r"]
    for i in valid_int:
        valid_choices.append(str(i))

    if choice in valid_choices:
        return choice

    else:
        display("error", "input_error")
        return menu_profile_display(wav_files)
    return


def menu_profile(_id_, wav_files, lastname, firstname):
    choice = menu_profile_display(wav_files)

    yes = get_message("menu_admin", "press_yes")
    no = get_message("menu_admin", "press_no")

    if choice == yes:
        print(get_message("menu_admin", "modification_permission"))
        change_permissions(int(_id_), True)
        display_user_by_id(_id_)
        menu_profile(_id_, wav_files, lastname, firstname)

    if choice == no:
        print(get_message("menu_admin", "modification_permission"))
        change_permissions(int(_id_), False)
        display_user_by_id(_id_)
        menu_profile(_id_, wav_files, lastname, firstname)

    if choice == "v":
        choice = menu_record_method_display()
        if choice == "1":
            # record voice sounds
            ok, files = record_user(lastname, firstname)
            if ok == False:
                display("menu_admin", "return_to_previous_menu")
                return

        elif choice == "2":
            files = []
            files2 = easygui.fileopenbox(
                title="Importing sound files", msg="Select sound files", multiple=True
            )
            if files2 == None:
                display("menu_admin", "return_to_previous_menu")
                return

            for file in files2:
                filename = "record_"
                filename += (
                    str(datetime.datetime.now())
                    .replace(" ", "_")
                    .replace(".", "-")
                    .replace(":", "-")
                )
                filename += ".wav"
                path = os.getcwd() + "/src/data/wav_files/" + filename
                shutil.copy(file, path)
                files.append(filename)

        # function for extracting vocal caracteristics here
        display("menu_admin", "extracting")
        vectors = []
        for file in files:
            path = "src/data/wav_files/" + file
            vectors.append((extractVector(path)).tolist())
        display("menu_admin", "extracted")

        # functions to identify the vector in either the blacklist or the directory
        for vector in vectors:
            found, _id2_ = compare_to_blacklist(vector)
            if found:
                print(get_message("error", "matched") + str(_id2_))
                display("error", "already_in_blacklist")
                remove_files(files)
                return
        for vector in vectors:
            found, _id2_ = compare_to_directory(vector)
            
            if found and (int(_id2_) != int(_id_)):
                print(get_message("error", "matched") + str(_id2_))
                display("error", "already_in_directory")
                remove_files(files)
                return

        display("menu_admin", "saving")

        """
        # getter on the list of old filenames for deletion
        item = search_user_by_id(int(_id_))
        if item == False:    
            print(get_message("error", "user_not_found_begin") + str(_id_) + get_message("error", "user_not_found_end"))
            return menu_directory()
        old_files = item["wav_files"]
        """

        # function to save data in user directory here
        ok = update_user_sound_recording(int(_id_), files, vectors)
        if ok == False:
            display("error", "error_during_save")
            remove_files(files)
            return

        display("menu_admin", "saved")
        # remove_files(old_files)  # to delete old .wav files

        wav_files, lastname, firstname = display_user_by_id(_id_)
        menu_profile(_id_, wav_files, lastname, firstname)

    if choice == "r":
        menu_directory()

    valid_int = list(range(1, len(wav_files) + 1))
    other_valid_choices = []
    for i in valid_int:
        other_valid_choices.append(str(i))

    if choice in other_valid_choices:
        play_wav(wav_files[int(choice) - 1])
        display_user_by_id(_id_)
        menu_profile(_id_, wav_files, lastname, firstname)

    return


def search_user():
    search_string = search_user_display()
    list_users_containing_search_string = search_users_by_keywords(search_string)
    display("menu_admin", "search_result")
    list_users(list_users_containing_search_string)
    return


def search_user_display():
    display("menu_admin", "search_user")
    search_string = input()
    while search_string == "":
        display("error", "input_error")
        return search_user_display()
    return search_string


####################### BLACKLIST ###################


def menu_blacklist_display():
    """display the blacklist menu options and returns the user's choice"""
    display("menu_admin", "menu_blacklist")
    choice = input()

    if choice == "r":
        return choice
    else:
        item = search_black_listed_person_by_id(int(choice))
        if item == False:
            print(
                get_message("error", "id_not_found_blacklist_begin")
                + str(choice)
                + get_message("error", "id_not_found_blacklist_end")
            )
            print("")
            return menu_blacklist_display()
        else:
            return choice


def menu_blacklist():
    people = search_all_black_listed_persons()
    sorted_people = sorted(
        people,
        key=lambda x: datetime.datetime.strptime(
            x["datetime_attempt"][len(x["datetime_attempt"]) - 1], "%d/%m/%Y %H:%M:%S"
        ),
        reverse=True,
    )
    list_blacklist(sorted_people)
    choice = menu_blacklist_display()
    if choice == "r":
        main()
    else:
        item = search_black_listed_person_by_id(int(choice))
        display_blacklisted_person(choice, item["datetime_attempt"], item["wav_files"])
        menu_blacklisted_person_profile(
            choice, item["datetime_attempt"], item["wav_files"]
        )


def menu_blacklisted_person_profile(_id_, list_access, list_audio_files):
    nb_recordings = len(list_audio_files)
    choice = menu_blacklisted_person_profile_display(nb_recordings)
    delete = get_message("menu_admin", "press_delete")

    if choice == "r":
        menu_blacklist()

    if choice == delete:
        deleted = confirm_deletion(_id_)

        if deleted:
            delete_black_listed_person(int(_id_))
            menu_blacklist()
        else:
            menu_blacklisted_person_profile(_id_, list_access, list_audio_files)

    else:
        play_wav(list_audio_files[nb_recordings - int(choice)])
        display_blacklisted_person(_id_, list_access, list_audio_files)
        menu_blacklisted_person_profile(_id_, list_access, list_audio_files)


def menu_blacklisted_person_profile_display(nb_recordings):
    display("menu_admin", "menu_blacklisted_person_profile")
    choice = input()
    valid_int = list(range(1, nb_recordings + 1))
    valid_choices = []
    delete = get_message("menu_admin", "press_delete")
    for i in valid_int:
        valid_choices.append(str(i))
    valid_choices.append(delete)
    valid_choices.append("r")

    if choice in valid_choices:
        return choice

    else:
        display("error", "input_error")
        return menu_blacklisted_person_profile_display(nb_recordings)
    return


def confirm_deletion(_id_):
    choice = confirm_deletion_display()

    yes = get_message("menu_admin", "press_yes")
    no = get_message("menu_admin", "press_no")

    if choice == yes:
        print("suppression de " + str(_id_) + " de la blacklist")
        delete_black_listed_person(_id_)
        return True

    if choice == no:
        print("Annulation de la suppression de " + str(_id_) + " de la blacklist")
        # does nothing
        return False


def confirm_deletion_display():
    display("menu_admin", "confirm_blacklist_delete")
    choice = input()
    yes = get_message("menu_admin", "press_yes")
    no = get_message("menu_admin", "press_no")
    valid_choices = [yes, no]

    if choice in valid_choices:
        return choice

    else:
        display("error", "input_error")
        return confirm_deletion_display()
    return


################## TEST IDENTIFICATION #################


def test_identification():
    choice = select_method_identification()
    if choice == "1":
        ok, file = record_test_identification()
        files = [file]

        if ok == False:
            display("menu_admin", "return_to_previous_menu")
            return

    if choice == "2":
        f = easygui.fileopenbox(
            title="Importing sound file", msg="Select sound files", multiple=False
        )
        files = []
        if f == None:
            display("menu_admin", "return_to_previous_menu")
            return

        file = "record_"
        file += (
            str(datetime.datetime.now())
            .replace(" ", "_")
            .replace(".", "-")
            .replace(":", "-")
        )
        file += ".wav"
        path = os.getcwd() + "/src/data/wav_files/" + file
        shutil.copy(f, path)
        files.append(file)

    # function for extracting vocal caracteristics here
    display("menu_admin", "extracting")
    path = "src/data/wav_files/" + file
    vector = extractVector(path)
    display("menu_admin", "extracted")

    # functions to identify the vector in either the blacklist or the directory
    found, _id = compare_to_blacklist(vector)
    if found:
        print("")
        print(get_message("error", "matched") + str(_id))
        display("menu_admin", "found_in_blacklist")
        item = search_black_listed_person_by_id(_id)
        print(get_message("menu_auth", "display_name") + " " + str(item["firstname"]) + " " + str(item["lastname"]))
        print("")

        remove_files(files)
        return

    found, _id = compare_to_directory(vector, debug=False)
    if found:
        print("")
        print(get_message("error", "matched") + str(_id))
        display("menu_admin", "found_in_directory")
        item = search_user_by_id(_id)
        print(get_message("menu_auth", "display_name") + " " + str(item["firstname"]) + " " + str(item["lastname"]))
        print("")

        remove_files(files)
        return

    display("menu_admin", "no_voice_match_found")
    print("")
    remove_files(files)
    return


def select_method_identification():
    display("menu_admin", "record_option")
    choice = input()
    valid_choices = ["1", "2"]
    if choice in valid_choices:
        return choice

    else:
        display("error", "input_error")
        return select_method_identification()
    return


####################### LANGUAGE ###################


def select_language():
    """menu to change the language of the application"""
    display("menu_admin", "select_language")
    while True:
        choice = input()
        valid_choices = ["1", "2"]
        if choice in valid_choices:
            if choice == "1":
                change_language("en")
                return
            elif choice == "2":
                change_language("fr")
                return
        else:
            display("error", "input_error")


############## INTEGRATION TESTING FUNCTIONS #############


# sound processing > sound extraction
"""
def extractVector(files):
    # some action on the wav files to extract the vectors
    return []
"""

# sound processing > identification

"""
def compare_to_blacklist(vectors):
    # true if found and return the ID, else false, None
    # print("comparaison avec blacklist")
    global criteria

    best_res = 9999
    accept = False
    _id_ = -1
    users = search_all_black_listed_persons()

    for user in users:
        print("\ncomparaison à l'id " + str(user["id"]))
        for vector in vectors:

            for v in user["vocal_print"]:
                cost = compare_vectors(vector, v)
                print("cout : " + str(cost))
                if cost < criteria:
                    accept = True
                    if cost < best_res:
                        best_res = cost
                        _id_ = user["id"]
    return accept, _id_
"""

"""
def compare_to_directory(vectors):
    # true if found and return the ID, else false, None
    print("comparaison avec annuaire")
    global criteria
    best_res = 999
    accept = False
    _id_ = -1
    users = search_all_users()
    for user in users:
        print("\ncomparaison à l'id " + str(user["id"]))
        for vector in vectors:
            for v in user["vocal_print"]:
                cost = compare_vectors(vector, v)
                print("cout : " + str(cost))
                if cost < criteria:
                    accept = True
                    if cost < best_res:
                        best_res = cost
                        _id_ = user["id"]
    return accept, _id_
"""

"""
def compare_to_others_in_directory(vectors, _self_id_):
    # true if found and return the ID, else false, None
    # does not compare to yourself
    print("comparaison avec annuaire")
    global criteria
    accept = False
    _id_ = -1
    users = search_all_users()
    for user in users:
        if user["id"] == int(_self_id_):
            continue
        print("\ncomparaison à l'id " + str(user["id"]))
        for vector in vectors:
            for v in user["vocal_print"]:
                cost = compare_vectors(vector, v)
                print("cout : " + str(cost))
                if cost < criteria:
                    accept = True
                    _id_ = user["id"]
    return accept, _id_

"""

"""
def compare_to_blacklist(vector):
    # true if found and return the ID, else false, None
    print("comparing to blacklist")
    return False, 42

def compare_to_directory(vector):
    # true if found and return the ID, else false, None
    print("comparing to annuaire")
    return False, 42
"""

# data management > annuaire
"""
def save_new_user(lastname, firstname, registration_id, files, vectors):
    # some action on the directory to save the user
    return True
"""
"""
def search_user_by_id(_id_): 
    # some action on the directory to fetch the corresponding user
    # can return False if no match found for the ID    
    # return item otherwise
    return False
"""
"""
def change_permissions(_id_, trueOrFalse):
    # some action on the directory
    return True
"""
"""
def update_user_sound_recording(_id_, files, vector):
    # some action on the directory
    return True
"""
"""
def search_string_in_directory(s):
    #search the directory and returns users that contains the string s as a list of users    
    return [1,2,3,4]
"""


# data management > blacklist

"""
def search_black_listed_person_by_id(_id_): 
    # some action on the directory to fetch the corresponding user
    # can return False, _ , _ , .... if no match found for the ID    
    if _id_ == "false": 
      return False, [], []
    d1 = datetime.datetime(2020, 9, 20, 21 , 48, 59, 848)    
    d2 = datetime.datetime(2020, 12, 10, 23 , 48, 59, 213548)          
    return True, [str(d1), str(d2)], ["ben_bravo.wav", "ben_bravo2.wav"]
"""

"""
def delete_black_listed_person(_id_):
    #delete the entry with the corresponding id from the blacklist
    return True
"""

"""execution """

main()
