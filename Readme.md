# Projet V-Access (Capgemini/Sorbonne Universite)

## Introduction
This is a guide to use V-Access and it will show you how to get started with the whole project.

V-Access allows you to:
- Register the vocal print of a person.
- Administrate a database of vocal print.
- Identify a user using its vocal print.

This guide is divided in two parts, the first part is how to deploy and use the application, the second part will cover the implementation and structure of the project in order for you to understand how the vocal print is extracted from an audio and how do we compare 2 vocal prints to each others.

## Get started 
1. Install Python 3.7.
2. Download the project.
3. Get to the root of the project with a terminal
4. Install dependencies using the command ```python -m pip install -r Requirements.txt```
5. Execute :
    - Administrator : ```python -m src.app.main_admin``` 
    - Authentificator : ```python -m src.app.main_auth```

## Architecture

### Data Structure

User data are store in two different JSON, a regular database "annuaire" and a blacklist. The recordings are processed with .wav files and are also saved in the folder src/data/wav_files.
Here's how the data are stored in the annuaire
```
"id": integer (example: 6)
"firstname": string (example: “John”)
"lastname": string (example: "Doe“)
"company_id": string (example: “VISITOR”)
"authorized": string of <boolean> (example: “true”)
"datetime_creation": string of <dateformat DD/MM/YYYY HH/MM/SS> (example: "26/01/2022 15:39:53")
"datetime_last_access": string of <dateformat DD/MM/YYYY HH/MM/SS>
"datetime_last_update": string of <dateformat DD/MM/YYYY HH/MM/SS>
"vocal_print": [ <float> ] (example: [-1446.27392578125,-1446.27392578125, …] )
"wav_files": [ string of wav filename ] (example: "record_2022-01-26_17-30-06-955180.wav")

projet_capgemini/src/data/voice/blacklist.json
"id": integer
"datetime_creation": string of <dateformat DD/MM/YYYY HH/MM/SS>
"datetime_attempt": [ string of <dateformat DD/MM/YYYY HH/MM/SS> ]
"vocal_print": [ <float> ]
"wav_files": [ string of wav filename ]
"vocal_print": [ <float> ]
```

## Extraction and comparison
### Extraction

The whole extraction process is in the file sound_extraction.py which is divided in two parts.
First part is a pre-treatment that normalize the amplitude of the signal so that if someone speaks loudly or gently he can be recognized.
```python
# Normalize
aAudio = aAudio / np.max(np.abs(aAudio))
```

The audio is then processed with the function mfcc of the Librosa library. It returns a matrix of mfccs.
There are some parameters that we can change in order to have a more precise extraction such as:
- n_mfccs: numbers of mfccs to return (13 by default), it is used to evaluate the value of some frequencies, the more you use, the more frequencies are evaluated.
- hop_length: numbers of segments in an audio (Current configuration is 256, for an audio of 3 seconds, it makes windows of ~11 ms per segment)
- dct_type: type of discrete cosinus transformation
- lifter: used to emphasize the values of mfccs index higher than lifter/2

Here's a signal with 13 mfccs and 256 segments.
![mfcc_explained](assets/mfcc_explained.jpeg)


There's also a debug mode that can be activated by passing the parameters debug=True, such as extractVector(myVector, debug=True). It allows for the application to print and also visualize graphs during the extraction of a signal.
```python
if debug:
    print("Sample Rate : ", nSampleRate)
    _, ax = plt.subplots(nrows=6, sharex=True)
    ld.waveshow(aAudio, sr=nSampleRate, ax=ax[0])
    ax[0].set(title="Before norm")
    ax[0].label_outer()
if debug:
    ld.waveshow(aAudio, sr=nSampleRate, ax=ax[1])
    ax[1].set(title="After norm")
    ax[1].label_outer()
if debug:
    plt.show()
    librosa.display.specshow(aVectorsCoeff, sr=44000)
    plt.xlabel("Time")
    plt.ylabel("MFCC")
    plt.colorbar()
    plt.show()
```


### Comparison

In order to compare two audios, we are going to generate a score from the distance between the audios.

To calculate the distance, we are using the DTW (Dynamic Time Warping) which tries to realign two matrices. The idea is to calculate the cost of realigning two matrix which can also be used as a distance. The lower it is, the closest it is. When using the DTW from Librosa, it returns a matrix, on the x axis are the segments of the first audio, on the y axis segments from the second audio. The values from each entry of the matrix stands for the cost between two segments. Then, the Warping Path is also returned which correspond to the optimal path to realign both segments with the lowest cost possible.

Here's how it looks like
![dtw_parfait](assets/dtw_parfait.png)

Each segment of the first audio is perfectly aligned to each segment of the second audio, and this is why you can visualize a diagonal. On top of that, the realignment cost is 0 so the color is dark representing a very low cost.

On the opposite side when two audios are extremely different, the warping path will show a line that isn't a diagonal and the values on the line will be very high.
![dtw_different](assets/dtw_different.png)

One of the methods to calculate the total cost is to calculate the cost for each segment of the optimal path.
```python
for i in range(len(wp[:, 1])):
    total_cost += mth.fabs(D[wp[i, 0], wp[i, 1]])
mean_cost = total_cost / len(wp[:, 1])
```

Second method is to calculate the best cost, which means the cost between the two last segments of each audio.
```python
best_cost = D[wp[-1, 0], wp[-1, 1]]
```

The component can return multiple values and can be either mean, best or both values.

To compare two people, in the current implementation, we first compare the best score, then the mean score, if one of them is correct, we accept the user as recognized. To authorise a user for the authentification, he has to be recognized to three entries in the database.
Acceptable cost for my computer has been configured as so:
- Best cost threshold: 12.
- Mean Cost threshold: 52 000.

Those values can be adjusted in the identification.py file.

Also, the identification functions can be called with the argument debug=True, which also prints values and allows you to visualize the comparison such as before.

## Technique tested

You can see that the cosines similarity has been tested and is still present within the code as the functions ```compare_vectors_cosine``` and ```test_compare_vectors_cosine```.
The idea was ton concatenate the matrix of mfccs as one vector and use the cosines similarity in order to compute a distance between the two matrices. Unfortunately, it has proven to be inefficient to differentiate two differents voices.

## Could be improved

As shown precedently, the similarity between two vectors can be calculated via the cost. Although it is an interesting approach to compute the distance between a diagonal starting from the first point of the warping path and see how the warping path is aligning or no to the said diagonal.

## Authors

- Jeremy Nguyen
- Thibault Roche
- Paul Nguyen dit Syvala
- Kaoutar Nhaila